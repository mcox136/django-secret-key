import os
import stat

from .base import (
    Secret,
    generate,
)


class SecretFromFile(Secret):
    """Class for obtaining a secret key from a file."""
    file_perms = (
        stat.S_IRUSR |
        stat.S_IWUSR |
        stat.S_IRGRP |
        stat.S_IWGRP
    )

    def __init__(self, filepath, *args, **kwargs):
        self.filepath = filepath
        super(SecretFromFile, self).__init__(*args, **kwargs)

    def reload(self):
        try:
            with open(self.filepath, 'r') as opfi:
                self._secret = opfi.read().strip()
        except IOError:
            self._create()
        else:
            self.key_length = len(self._secret)
        return self._secret

    def _create(self):
        secret = generate(self.key_length)
        with open(self.filepath, 'w') as opfi:
            opfi.write(secret)

        os.chmod(self.filepath, self.file_perms)
        self._secret = secret
