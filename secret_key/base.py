from random import SystemRandom


CHOICE_STRING = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'


def generate(length):
    gen = SystemRandom()
    return ''.join(gen.choice(CHOICE_STRING) for i in range(length))


class Secret(object):
    """Base class for secret key objects."""
    def __init__(self, key_length=50):
        self.key_length = key_length
        self._secret = None

    def load(self):
        if self._secret is None:
            self._secret = self.reload()
        return self._secret
