os = None


class SecretFromEnvVar(Secret):
    """Class for obtaining a secret key from an environment variable."""
    def __init__(self, varname, fail_silently, *args, **kwargs):
        self.varname = varname
        self.fail_silently = fail_silently
        super(SecretFromEnvVar, self).__init__(*args, **kwargs)

    def reload(self):
        global os
        if os is None:
            import os

        if self.fail_silently:
            self._secret = os.environ[self.varname]
            return self._secret

        try:
            self._secret = os.getenv(self.varname)
        except KeyError:
            os.environ[self.varname] = self._secret = generate(self.key_length)
        else:
            self.key_length = len(self._secret)
        return self._secret
