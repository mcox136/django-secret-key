import os
import stat

from unittest import TestCase

from .compat import mock

from secret_key.from_file import SecretFromFile


class FromFileTest(TestCase):
    def setUp(self):
        self.keyfile = 'secret-key-test-file.txt'

    def tearDown(self):
        if os.path.exists(self.keyfile):
            os.remove(self.keyfile)

    def test_reload_writes_new_key_to_keyfile_if_nonexistent(self):
        secret_obj = SecretFromFile(self.keyfile)
        secret_string = secret_obj.reload()
        self.assertTrue(os.path.exists(self.keyfile))

        with open(self.keyfile, 'r') as opfi:
            contents = opfi.read()

        self.assertEqual(contents.strip(), secret_string)

    def test_reload_does_not_overwrite_existing_file(self):
        secret_string = 'some-secret-key'
        with open(self.keyfile, 'w') as opfi:
            opfi.write(secret_string)

        secret_obj = SecretFromFile(self.keyfile)
        self.assertEqual(secret_obj.load(), secret_string)

    def test_file_permissions_are_read_write_user_and_group_only(self):
        SecretFromFile(self.keyfile).reload()
        result = os.stat(self.keyfile)

        enabled = [stat.S_IRUSR, stat.S_IWUSR, stat.S_IRGRP, stat.S_IWGRP]
        disabled = [stat.S_IXUSR, stat.S_IXGRP, stat.S_IXOTH, stat.S_IWOTH,
                    stat.S_IROTH]

        self.assertTrue(all(result.st_mode & mode for mode in enabled))
        self.assertFalse(any(result.st_mode & mode for mode in disabled))
