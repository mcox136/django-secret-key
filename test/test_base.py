from unittest import TestCase

from .compat import mock

from secret_key.base import (
    generate,
    CHOICE_STRING,
    Secret,
)


class GenerateTestCase(TestCase):
    def test_returns_string_of_desired_length(self):
        length = 50
        self.assertEqual(len(generate(length)), length)

    def test_returns_string_composed_from_choices(self):
        string = generate(50)
        self.assertTrue(all(c in CHOICE_STRING for c in string))

    def test_returns_random_string(self):
        self.assertNotEqual(generate(50), generate(50))


class DerivedSecret(Secret):
    def reload():
        pass


class SecretTestCase(TestCase):
    def test_does_not_call_reload_if_load_is_unused(self):
        with mock.patch.object(DerivedSecret, 'reload') as reload:
            secret_obj = DerivedSecret()
            reload.assert_not_called()

    def test_reloads_exactly_once(self):
        with mock.patch.object(DerivedSecret, 'reload') as reload:
            secret_obj = DerivedSecret()
            secret_obj.load()
            secret_obj.load()
            self.assertEqual(secret_obj.reload.call_count, 1)

    def test_returns_the_reload_result(self):
        secret_obj = DerivedSecret()
        with mock.patch.object(secret_obj, 'reload', return_value=42):
            self.assertEqual(secret_obj.load(), 42)
