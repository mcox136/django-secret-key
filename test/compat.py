try:
    # Python 3.
    from unittest import mock
except ImportError:
    # Python 2; requires independent installation of mock module.
    import mock
