django-secret-key
=================

Utilities for creating and loading secret keys.


Development
-----------
Use the ``[dev]`` suffix to install development libraries:

    pip install -e $REPOSITORY_URL[dev]

Tests can then be run with PyTest:

    py.test

Usage
-----
To load a secret key from a file:

    from secret_key.from_file import SecretFromFile

    secret_key_obj = SecretFromFile(filepath='my-secret-key.txt')
    secret_key_str = secret_key_obj.load()

If the key file doesn't exist, one is created and a new key inserted.
